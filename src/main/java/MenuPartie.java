import java.util.*;


public class MenuPartie {

    public static String nomPourResultat;


    public static void avertissement() {
        System.out.println("!! Attention, le chronomètre de la partie commence quand vous validez votre première proposition !!");
    }
    /*cette méthode pour empêcher l'utilisateur d'avoir le nom "exit" avec ou sans majuscule
           ou d'avoir " : " dans son pseudo
            */
    public static String nomRefuse() throws Exception {
         while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Votre Nom : ");
            String nomJoueur = scanner.nextLine();
            String nomInterdit = "exit";
            if (nomJoueur.toLowerCase().equals(nomInterdit) || nomJoueur.contains(":")) {
                System.out.println("Ce nom est interdit. Veuillez saisir un autre nom");
            } else {
                nomPourResultat = nomJoueur; // je stocke le nom de la variable NomPourResultat
                donnéesQueJeVeuxReutiliser.setNomPourResultat(nomPourResultat);
                chronoPartie();
                return nomPourResultat;
            }
        }
    }

    /* Cette méthode lance un chronomètre en lancant la partie, prend en paramètre la classe JustePrix.java
         qu'elle utilise et stop le chronomètre à la fin de son utilisation
          */
    public static float chronoPartie() throws Exception {
        long debutChrono = System.currentTimeMillis();
        JustePrix.jeuPrincipal();
        long finChrono = System.currentTimeMillis();
        float chronoPartie = (finChrono - debutChrono) / 1000F;
        donnéesQueJeVeuxReutiliser.setChrono(chronoPartie); // je stocke la valeur de chrono dans donnéesQueJeVeuxReutiliser.java
        JustePrix.victoirePartie(); //affiche l'annonce de victoire et le score de la victoire
        return chronoPartie;
    }
}

