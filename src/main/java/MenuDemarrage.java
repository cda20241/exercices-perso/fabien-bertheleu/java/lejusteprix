
import java.io.*;
import java.util.Scanner;

public class MenuDemarrage {

    public static void logoBienvenue() {
        System.out.println("###############################################################################################");
        System.out.println("###############################################################################################");
        System.out.println("#####                                                                                     #####");
        System.out.println("#####                               Bienvenue au                                          #####");
        System.out.println("#####                                Juste Prix                                           #####");
        System.out.println("#####                                                                                     #####");
        System.out.println("###############################################################################################");
        System.out.println("###############################################################################################");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    /**Je créé une fonction qui lit les lignes de mon fichier TXT pour ensuite imprimer les 10 dernières lignes
     *
     * @throws Exception
     */
    public static void CompteurLignesFichierTXT() throws Exception {
        File file = new File("ResultatsJoueurs.txt");
        int nbrLine = 0; // nombre magique : je met un compteur de nombre de lignes à 0
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String str;
        while ((str = br.readLine()) != null) {
            nbrLine++;// Pour chaque ligne comptée, je l'ajoute au compteur
        }
        fr.close();
        System.out.println("Le nombre de lignes dans le fichier est: " + nbrLine);
        // Lire les 10 dernières lignes
        RandomAccessFile fileHandler = new RandomAccessFile(file, "r");
        long fileLength = file.length() - 1;
        StringBuilder sb = new StringBuilder();
        int ligne = 0;
        for (long filePointer = fileLength; filePointer != -1; filePointer--) {
            fileHandler.seek(filePointer);
            int readByte = fileHandler.readByte();
            if (readByte == 0xA) {
                if (filePointer < fileLength) {
                    ligne = ligne + 1;
                }
            } else if (readByte == 0xD) {
                if (filePointer < fileLength - 1) {
                    ligne = ligne + 1;
                }
            }
            if (ligne >= 10) {//je choisis 10 pour les 10 dernières lignes, comme dans l'ennoncé
                break;
            }
            sb.append((char) readByte);
        }
        String last10Lines = sb.reverse().toString();
        System.out.println("Les 10 dernières lignes du fichier sont: ");
        System.out.println(last10Lines);// j'imprime la liste des 10 dernières lignes
        Scanner scanner = new Scanner(System.in);// je fais un scanner pour relancer la lecture après la touche entrée
        scanner.nextLine();
        afficheChoixMenuDemarrage();
    }


    public static void afficheChoixMenuDemarrage() throws Exception {
        System.out.println("###############################################################################################");
        System.out.println("###############################################################################################");
        System.out.println("#####                                                                                     #####");
        System.out.println("#####       1. Afficher le Tableau des Scores                                             #####");
        System.out.println("#####       2. Lancer une partie                                                          #####");
        System.out.println("#####       3. Quitter le jeu                                                             #####");
        System.out.println("#####                                                                                     #####");
        System.out.println("###############################################################################################");
        System.out.println("###############################################################################################");
        choixMenuPrincipal();
    }

    public static void menuOutro(){
        System.out.println("###############################################################################################");
        System.out.println("###############################################################################################");
        System.out.println("#####                                                                                     #####");
        System.out.println("#####                       Merci d'avoir joué au Juste Prix                              #####");
        System.out.println("#####                                                                                     #####");
        System.out.println("#####                                   A bientôt !                                       #####");
        System.out.println("#####                                                                                     #####");
        System.out.println("###############################################################################################");
        System.out.println("###############################################################################################");
        System.exit(69);

    }
    public static void choixMenuPrincipal() throws Exception {
        Scanner scanner = new Scanner(System.in);
        int choix = scanner.nextInt();
        switch (choix) {
                case 1:
                    Resultats.menuResultats();
                    break;
                case 2:
                    MenuPartie.avertissement();
                    break;
                case 3:
                    menuOutro();
                    break;
                default:
                    afficheChoixMenuDemarrage();
                    break;
            }
        }

}
