import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;
import java.util.List;
public class JustePrix {

    public static int scoreFinal;
    public static ArrayList<Integer> tousLesMontantsDonnes;

    public static ArrayList<Integer> getTousLesMontantsDonnes() {
        return tousLesMontantsDonnes;
    }

    public static void setTousLesMontantsDonnes(ArrayList<Integer> tousLesMontantsDonnes) {
        JustePrix.tousLesMontantsDonnes = tousLesMontantsDonnes;
    }

    public static void jeuPrincipal() throws Exception {
        Random rand = new Random();
        int nombreAleatoire = rand.nextInt(10001); //je créé un nombre aléatoir entre 0 et 10000
        //Je demande à l'utilisateur  le premier Prix qu'il veut proposer
        System.out.print("Votre Prix : ");
        Scanner scanner = new Scanner(System.in);
        int montantDonne = scanner.nextInt();
        //j'ajoute le premier montant que vient de me donner l'utilisateur à une liste
        ArrayList<Integer> tousLesMontantsDonnes = new ArrayList<>();
        tousLesMontantsDonnes.add(montantDonne);

        /**je créé une boucle for qui vérifie à chaque fois si le prix est juste.
         * Si Négatif, cela incrémente une liste où il y a toutes les entrées de la partie
         * (et en écrivant ça, je me dis que j'aurais peut être pu faire aec un boolean si prix juste, si prix faux avec mon if dedans)
         */
        for (int compteurBoucle = 0; nombreAleatoire > tousLesMontantsDonnes.get(compteurBoucle) || nombreAleatoire < tousLesMontantsDonnes.get(compteurBoucle); compteurBoucle++) {
            if (nombreAleatoire > tousLesMontantsDonnes.get(compteurBoucle)) {
                for (int i = 0; i < 20; i++) {
                    System.out.println(" ");
                }
                System.out.println(tousLesMontantsDonnes.get(compteurBoucle) + " €. C'est plus.");
            } else {
                for (int i = 0; i < 20; i++) {
                    System.out.println(" ");
                }
                System.out.println(tousLesMontantsDonnes.get(compteurBoucle) + " €. C'est moins. ");
            }
            int nouveauMontant = scanner.nextInt();
            tousLesMontantsDonnes.add(nouveauMontant);
        }
        scoreFinal = tousLesMontantsDonnes.size();
        donnéesQueJeVeuxReutiliser.setScoreFinal(scoreFinal);
        setTousLesMontantsDonnes(tousLesMontantsDonnes);


    }

    /** Cette méthode pour un affichage de la fin de partie et du résultat
     * incluant le nombre de tentatives et l'énumération de toutes les tentatives
     * la fin renvoi au menu de démarrage
     * @throws Exception
     */
    public static void victoirePartie () throws Exception {
        System.out.println("###############################################################################################");
        System.out.println("#####                                                                                     #####");
        System.out.println("#####                             C'est le JUSTE PRIX !!                                  #####");
        System.out.println("#####                           En seulement "+donnéesQueJeVeuxReutiliser.getScoreFinal() + " tentatives                                #####");
        System.out.println("#####                                                                                     #####");
        System.out.println("###############################################################################################");
        System.out.println("Vous avez réussi en "+ donnéesQueJeVeuxReutiliser.getChrono()
                                +" secondes avec les "
                                + +donnéesQueJeVeuxReutiliser.getScoreFinal()
                                + " essais suivants : ");
        System.out.println(getTousLesMontantsDonnes());
        System.out.println("Appuyez sur Entrée pour continuer");
        Resultats.classerResultatsTXT();
        Resultats.classerResultatsJson();
        Scanner scan = new Scanner(System.in);
        scan.nextLine();
        MenuDemarrage.afficheChoixMenuDemarrage();
            }
}

