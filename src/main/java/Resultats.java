import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Resultats {

    public static List<String> listeResultatPartie;
    private static String recherche;
    public String JustePrix;

    //      getter      //
    public static List<String> getListeResultatPartie() {
        return listeResultatPartie;
    }

    //      setter      //

    public static void setListeResultatPartie(List<String> listeResultatPartie) {
        Resultats.listeResultatPartie = listeResultatPartie;
    }

    public void setJustePrix(String justePrix) {
        JustePrix = justePrix;
    }

    @SuppressWarnings("rawtypes")
    public static void listeresultats() {
        /**Je créé une liste avec les résultats de la dernière partie
         */
        ArrayList listeResultatPartie = new ArrayList();
        listeResultatPartie.add(donnéesQueJeVeuxReutiliser.getNomPourResultat());
        listeResultatPartie.add(donnéesQueJeVeuxReutiliser.getScoreFinal());
        listeResultatPartie.add(donnéesQueJeVeuxReutiliser.getChrono());
        System.out.println(listeResultatPartie);
        setListeResultatPartie(listeResultatPartie);
    }

    /**j'écris les résultats de la dernière partie dans un fichier txt
     *on ajoute le boolean pour ne pas écrire par dessus le texte déjà existant
     */
    public static void classerResultatsTXT() {
        try {
            FileWriter fw = new FileWriter("ResultatsJoueurs.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            String lineToAppend = donnéesQueJeVeuxReutiliser.getNomPourResultat()
                                    + ", " +donnéesQueJeVeuxReutiliser.getScoreFinal()
                                    + ", " +String.valueOf(donnéesQueJeVeuxReutiliser.getChrono());
            bw.write(lineToAppend);
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    /**Cette méthode sert à charger les données dans le fichier json pour pouvoir ensuite l'utiliser, le but étant
     * d'extraire les données dans cette méthode, et les ajouter aux nouvelles données dans une nouvelle méthode
     * pour avoir ainsi un nouveau fichier json complet
     * @param nomFichier
     * @return joueurs
     * @throws IOException
     * @throws ParseException
     */
    public static JSONArray ChargerDonneeJson(String nomFichier) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        JSONArray joueurs = new JSONArray();
        try {
            File fichierJson = new File(nomFichier);
            // Si le fichier existe, le charger
            if (fichierJson.exists()) {
                try (FileReader reader = new FileReader(fichierJson)) {
                    Object obj = jsonParser.parse(reader);
                    joueurs = (JSONArray) obj;
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return joueurs;
    }



    /**après avoir récupéré les données dans la méthode recupDonnéesJson, j'ajoute les données de la dernière partie
     *en cours, et je ré-écris le tableau mis à jour dans le fichier json
     */
    public static void classerResultatsJson() throws IOException, ParseException {
        JSONArray joueurs = ChargerDonneeJson("ResultatsJoueurs.json");
        // je créé un tableau JSON pour stocker le joueur de la partie en cours
        JSONObject joueur = new JSONObject();
        joueur.put("nom", donnéesQueJeVeuxReutiliser.getNomPourResultat());
        joueur.put("score", donnéesQueJeVeuxReutiliser.getScoreFinal());
        joueur.put("chrono", donnéesQueJeVeuxReutiliser.getChrono());
        // j'ajoute le joueur au tableau de tous les joueurs
        joueurs.add(joueur);
        try (FileWriter fw = new FileWriter("ResultatsJoueurs.json");
             BufferedWriter bw = new BufferedWriter(fw)) {
            // j'écris le tableau des joueurs dans le fichier
            bw.write(joueurs.toJSONString());
            bw.newLine(); // Passer à la ligne pour le joueur suivant
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    /**Cette méthode pour lire le fichier TXT et l'afficher dans la console
     *
     * @throws Exception
     */
    public static void lectureTousLesResultatsTXT() throws Exception {
        File doc = new File("ResultatsJoueurs.txt");
        BufferedReader obj = new BufferedReader(new FileReader(doc));
        String strng;
        while ((strng = obj.readLine()) != null) System.out.println(strng);
    }

    /**Cette méthode pour lire le fichier Json et l'afficher dans la console
     *
     * @throws Exception
     */
    public static void lectureTousLesResultatsJson() {
        try (FileReader fileReader = new FileReader("ResultatsJoueurs.json")) {
            JSONParser jsonParser = new JSONParser();
            JSONArray joueurs = new JSONArray();
            // je lis le fichier json ligne par ligne, et ensuite, je lui demande de sortir les informations
            //que je veux dans l'ordre que je veux
            try (BufferedReader reader = new BufferedReader(fileReader)) {
                String line;
                while ((line = reader.readLine()) != null) {
                    //après lecture de la ligne, j'extraie chaque information pour la mettre dans une variable temporaire
                    //pour pouvoir ensuite l'afficher dans la console
                    JSONObject joueur = (JSONObject) jsonParser.parse(line);
                    String nom = (String) joueur.get("nom");
                    int score = Integer.parseInt(String.valueOf(joueur.get("score")));
                    float chrono = Float.parseFloat(String.valueOf(joueur.get("chrono")));
                    System.out.println("Nom : " + nom + ", Score : " + score + ", Chrono : " + chrono);
                }
            } catch (IOException | ParseException e) {
                throw new RuntimeException(e);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void menuResultats () throws Exception {
        System.out.println("################################################");
        System.out.println("###            TABLEAU DES SCORES            ###");
        System.out.println("################################################");
        System.out.println(" ");
        System.out.println("Nom du Joueur, Nombre de tentatves, Chronomètre");
        lectureTousLesResultatsTXT();
        while (true) {
            //je crée une boucle pour vérifier si l'utilisateur saisi exit, sinon, je passe à la recherche de joueur
            System.out.println("Vous cherchez un joueur précis ? Saisissez son nom ou 'exit' pour revenir au menu principal");
            Scanner scanner = new Scanner(System.in);
            String recherche = scanner.nextLine();
            if ("exit".equalsIgnoreCase(recherche)) {
                MenuDemarrage.afficheChoixMenuDemarrage();
                break;
        }
        // Charger les données depuis le fichier JSON
        JSONArray joueurs = ChargerDonneeJson("ResultatsJoueurs.json");

        // Afficher les résultats filtrés
        afficherResultatsFiltres(joueurs, recherche);
    }
}



    /**
     * Cette méthode permet d'afficher le nom que l'on recherche dans la liste. Cela affiche plus précisément les noms
     * qui contiennent la ou les lettres de la recherche.
     * @param joueurs
     * @param recherche qui vient du scanner de menuResultat()
     */
    public static void afficherResultatsFiltres(JSONArray joueurs, String recherche) {
        for (Object joueurObj : joueurs) {
            JSONObject joueur = (JSONObject) joueurObj;
            String nom = (String) joueur.get("nom");
            if (nom.toLowerCase().contains(recherche.toLowerCase())) {
                int score = Integer.parseInt(String.valueOf(joueur.get("score")));
                float chrono = Float.parseFloat(String.valueOf(joueur.get("chrono")));
                System.out.println(nom + ", " + score + ", " + chrono);
            }
        }
    }

    }




