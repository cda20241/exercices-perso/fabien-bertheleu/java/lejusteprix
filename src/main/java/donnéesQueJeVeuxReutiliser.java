public class donnéesQueJeVeuxReutiliser {
    private static float chrono;

    private static int scoreFinal;
    private static String nomPourResultat;

    //      GETTER      //

    public static float getChrono() {
        return chrono;
    }
    public static int getScoreFinal() {
        return scoreFinal;
    }

    public static String getNomPourResultat() {
        return nomPourResultat;
    }

    //      SETTER      //
    public static void setChrono(float value) {
        chrono = value;
    }

    public static void setScoreFinal(int scoreFinal) {
        donnéesQueJeVeuxReutiliser.scoreFinal = scoreFinal;
    }

    public static void setNomPourResultat(String nomPourResultat) {
        donnéesQueJeVeuxReutiliser.nomPourResultat = nomPourResultat;
    }
}
